package com.example.stealthmax.stealthapp;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button bStud;
    Button bPrep;
    ListView listView;
    static ArrayList parseList;
    ArrayAdapter adapter;
    public String mode="prep";

    static final String API_URl = "http://box1.binarus.ru:7088/TimeTableBD/api/PrepodInst.json";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bStud = (Button)findViewById(R.id.bStud);
        bPrep = (Button)findViewById(R.id.bStud);
        listView = (ListView)findViewById(R.id.listView);
        parseList = new ArrayList();
        parseList.add("Привет");
        adapter = new ArrayAdapter (this, android.R.layout.simple_list_item_1, parseList);
        listView.setAdapter(adapter);
        bStud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode="stud";
                new RetrieveFeedTask().execute();
            }
        });
        bPrep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode="prep";
                new RetrieveFeedTask().execute();
            }
        });

    }
    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected String doInBackground(Void... urls) {

            try {
                URL url = new URL(API_URl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{

                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if(response == null) {
                response = "THERE WAS AN ERROR";
            }

 parseList.add("kek");
            try {
                JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                for (int i=0; i<object.length(); i++) {
                    parseList.add(object.getString("prepodInsts"));

                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {

                // Appropriate error handling code
            }
            adapter.notifyDataSetChanged();
        }
    }
}
